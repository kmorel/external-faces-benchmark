//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2017 Sandia Corporation.
//  Copyright 2017 UT-Battelle, LLC.
//  Copyright 2017 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================

#include <vtkm/Version.h>


#include <vtkm/cont/CellSetPermutation.h>
#include <vtkm/cont/CellSetSingleType.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/RuntimeDeviceTracker.h>
#include <vtkm/cont/Timer.h>

#include <vtkm/filter/CleanGrid.h>
#include <vtkm/filter/Tetrahedralize.h>

#include <vtkm/testing/OptionParser.h>

#include "ExternalFacesHashFightFnv1a.h"
#include "ExternalFacesHashFightIdXor.h"
#include "ExternalFacesHashFightMorton.h"
#include "ExternalFacesSortFnv1a.h"
#include "ExternalFacesSortId3.h"
#include "ExternalFacesSortIdXor.h"
#include "ExternalFacesSortMorton.h"

#include "YamlWriter.h"

#include <ctime>
#include <iomanip>
#include <random>
#include <sstream>
#include <vector>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

static vtkm::testing::option::ArgStatus CheckIntArg(
    const vtkm::testing::option::Option &option, bool printMsg)
{
  if (!option.arg)
  {
    if (printMsg)
    {
      std::cerr << "Option " << option.name << " requires an integer option."
                << std::endl;
    }
    return vtkm::testing::option::ARG_ILLEGAL;
  }

  try
  {
    std::stoi(option.arg);
  }
  catch (std::invalid_argument)
  {
    if (printMsg)
    {
      std::cerr << "Option " << option.name << " given arg " << option.arg
                << ", which is not an integer." << std::endl;
    }
    return vtkm::testing::option::ARG_ILLEGAL;
  }
  return vtkm::testing::option::ARG_OK;
}

vtkm::cont::DataSet CreateDataSet(const vtkm::Id3 gridSize)
{
  vtkm::cont::DataSet baseGrid =
      vtkm::cont::DataSetBuilderUniform::Create(gridSize);

  vtkm::filter::Tetrahedralize tetrahedralize;
  /*vtkm::filter::Result result = tetrahedralize.Execute(baseGrid);*/

  return tetrahedralize.Execute(baseGrid);

  //return result.GetDataSet();
}

struct ReverseOneToOneMap : vtkm::worklet::WorkletMapField
{
  typedef void ControlSignature(FieldIn<> forwardMap,
                                WholeArrayOut<> reverseMap);
  typedef void ExecutionSignature(InputIndex, _1, _2);

  template<typename ReverseMapPortalType>
  VTKM_EXEC
  void operator()(vtkm::Id originalIndex,
                  vtkm::Id newIndex,
                  ReverseMapPortalType &reverseMapPortal) const
  {
    reverseMapPortal.Set(newIndex, originalIndex);
  }
};

struct TransformCellConnections : vtkm::worklet::WorkletMapPointToCell
{
  typedef void ControlSignature(CellSetIn cellSet,
                                WholeArrayIn<> pointMap,
                                FieldOutCell<> shapeOut,
                                FieldOutCell<> indicesOut);
  typedef void ExecutionSignature(PointIndices, CellShape, _2, _3, _4);

  template<typename PointIndicesVecType,
           typename ShapeTag,
           typename PointMapPortalType,
           typename IndicesVecType>
  VTKM_EXEC
  void operator()(const PointIndicesVecType &pointIndicesVec,
                  ShapeTag shape,
                  const PointMapPortalType &pointMapPortal,
                  vtkm::UInt8 &shapeOut,
                  IndicesVecType &indicesOutVec) const
  {
    shapeOut = shape.Id;

    vtkm::IdComponent numPoints = pointIndicesVec.GetNumberOfComponents();
    VTKM_ASSERT(numPoints == indicesOutVec.GetNumberOfComponents());
    for (vtkm::IdComponent index = 0; index < numPoints; index++)
    {
      indicesOutVec[index] = pointMapPortal.Get(pointIndicesVec[index]);
    }
  }
};

vtkm::cont::DataSet RandomizeDataSet(vtkm::cont::DataSet &inDataSet,
                                     YamlWriter &log)
{
  using Algorithm =
  vtkm::cont::DeviceAdapterAlgorithm<VTKM_DEFAULT_DEVICE_ADAPTER_TAG>;

  vtkm::cont::DataSet outDataSet;

  std::mt19937 randomGenerator;
  vtkm::UInt32 seed = static_cast<vtkm::UInt32>(std::time(nullptr));
  log.AddDictionaryEntry("randomize-seed", seed);
  randomGenerator.seed(seed);

  std::uniform_int_distribution<vtkm::Id> randomDist;

  //vtkm::cont::ArrayHandleUniformPointCoordinates originalCoords;
  //inDataSet.GetCoordinateSystem().GetData().CopyTo(originalCoords);
  auto originalCoords = inDataSet.GetCoordinateSystem().GetData();
  vtkm::Id numPoints = originalCoords.GetNumberOfValues();

  std::vector<vtkm::Id> pointMapBuffer(numPoints);
  for (std::size_t index = 0; index < static_cast<std::size_t>(numPoints); index++)
  {
    vtkm::Id swapIndex = randomDist(randomGenerator)%(index+1);
    pointMapBuffer[index] = pointMapBuffer[swapIndex];
    pointMapBuffer[swapIndex] = index;
  }
  vtkm::cont::ArrayHandle<vtkm::Id> pointMapNewToOld =
      vtkm::cont::make_ArrayHandle(pointMapBuffer);

  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault,3> > randomizedCoords;
  Algorithm::Copy(
        vtkm::cont::make_ArrayHandlePermutation(pointMapNewToOld, originalCoords),
        randomizedCoords);
  outDataSet.AddCoordinateSystem(
        vtkm::cont::CoordinateSystem("coords",
                                     randomizedCoords));

  vtkm::cont::ArrayHandle<vtkm::Id> pointMapOldToNew;
  pointMapOldToNew.PrepareForOutput(numPoints,
                                    VTKM_DEFAULT_DEVICE_ADAPTER_TAG());

  vtkm::worklet::DispatcherMapField<ReverseOneToOneMap, VTKM_DEFAULT_DEVICE_ADAPTER_TAG>
      reversePointMapDispatcher;
  reversePointMapDispatcher.Invoke(pointMapNewToOld, pointMapOldToNew);

  vtkm::cont::CellSetSingleType<> inCellSet;
  inDataSet.GetCellSet().CopyTo(inCellSet);
  vtkm::Id numCells = inCellSet.GetNumberOfCells();

  std::vector<vtkm::Id> cellMapBuffer(numCells);
  for (std::size_t index = 0; index < static_cast<std::size_t>(numCells); index++)
  {
    vtkm::Id swapIndex = randomDist(randomGenerator)%(index+1);
    cellMapBuffer[index] = cellMapBuffer[swapIndex];
    cellMapBuffer[swapIndex] = index;
  }
  vtkm::cont::ArrayHandle<vtkm::Id> cellMap =
      vtkm::cont::make_ArrayHandle(cellMapBuffer);

  vtkm::cont::ArrayHandle<vtkm::UInt8> outShapes;
  vtkm::cont::ArrayHandle<vtkm::Id> randomizedConnections;
  randomizedConnections.PrepareForOutput(
        inCellSet.GetNumberOfPointsInCell(0)*inCellSet.GetNumberOfCells(),
        VTKM_DEFAULT_DEVICE_ADAPTER_TAG());

  vtkm::worklet::DispatcherMapTopology<TransformCellConnections, VTKM_DEFAULT_DEVICE_ADAPTER_TAG>
      transformDispatcher;
  transformDispatcher.Invoke(
        vtkm::cont::make_CellSetPermutation(cellMap, inCellSet),
        pointMapOldToNew,
        outShapes,
        vtkm::cont::make_ArrayHandleGroupVecVariable(
          randomizedConnections,
          vtkm::cont::make_ArrayHandleCounting(
            0,
            inCellSet.GetNumberOfPointsInCell(0),
            inCellSet.GetNumberOfCells())));

  vtkm::cont::CellSetSingleType<> randomizedCellSet(inCellSet.GetName());
  randomizedCellSet.Fill(inCellSet.GetNumberOfPoints(),
                         inCellSet.GetCellShape(0),
                         inCellSet.GetNumberOfPointsInCell(0),
                         randomizedConnections);

  outDataSet.AddCellSet(randomizedCellSet);

  return outDataSet;
}

template<typename ExternalFacesWorklet>
vtkm::Float64 RunTrial(ExternalFacesWorklet externalFaces,
                       const vtkm::cont::DataSet &inData,
                       YamlWriter &log,
                       bool firstRun = false)
{
  vtkm::cont::DynamicCellSet dynamicCellSet = inData.GetCellSet();
  vtkm::cont::CellSetSingleType<> inCellSet;
  dynamicCellSet.CopyTo(inCellSet);

  vtkm::cont::CellSetExplicit<> outCellSet;

  std::stringstream dummyStream;
  YamlWriter dummyLog(dummyStream);

  vtkm::cont::Timer<> timer;
  externalFaces.Run(inCellSet,
                    inData.GetCoordinateSystem(),
                    outCellSet,
                    firstRun ? dummyLog : log,
                    VTKM_DEFAULT_DEVICE_ADAPTER_TAG());
  vtkm::Float64 elapsedTime = timer.GetElapsedTime();

  if (firstRun)
  {
    log.AddDictionaryEntry(
          "num-output-faces",
          outCellSet.GetNumberOfCells());
  }
  return elapsedTime;
}

template<typename ExternalFacesWorklet>
void DoRun(ExternalFacesWorklet externalFaces,
           const std::string &algorithmName,
           const std::string &hashName,
           vtkm::IdComponent numTrials,
           const vtkm::cont::DataSet &inData,
           YamlWriter &log)
{
  log.StartListItem();
  log.AddDictionaryEntry("algorithm-name", algorithmName);
  log.AddDictionaryEntry("hash-name", hashName);
  log.AddDictionaryEntry("full-name", algorithmName + " " + hashName);

  log.AddDictionaryEntry("first-run-time",
                         RunTrial(externalFaces, inData, log, true));

  log.StartBlock("trials");
  for (vtkm::IdComponent trial = 0; trial < numTrials; trial++)
  {
    log.StartListItem();
    log.AddDictionaryEntry("trial-index", trial);
    log.AddDictionaryEntry("seconds-total",RunTrial(externalFaces,inData,log));
  }
  log.EndBlock();
}

int main(int argc, char *argv[])
{
  enum OptionEnum {
    UNKNOWN,
    HELP,
    TRIALS,
    SIZE,
    SIZEX,
    SIZEY,
    SIZEZ,
    RANDOMIZE_CONNECTIONS
  };
  using namespace vtkm::testing;
  const option::Descriptor usage[] = {
    {UNKNOWN,   0, "",  "",       option::Arg::None, "USAGE: external-faces-benchmark [options]\n\nOptions:"},
    {HELP,      0, "h", "help",   option::Arg::None, "  --help       Print usage and exit"},
    {TRIALS,    0, "",  "trials", CheckIntArg,       "  --trials <N> Run for N trials (default 5)"},
    {SIZE,      0, "",  "size",   CheckIntArg,       "  --size <N>   Make a grid of size NxNxN (default 100x100x100)"},
    {SIZEX,     0, "",  "sizeX",  CheckIntArg,       "  --sizeX <N>  Make a grid of size N in the X direction"},
    {SIZEY,     0, "",  "sizeY",  CheckIntArg,       "  --sizeY <N>  Make a grid of size N in the Y direction"},
    {SIZEZ,     0, "",  "sizeZ",  CheckIntArg,       "  --sizeZ <N>  Make a grid of size N in the Z direction"},
    {RANDOMIZE_CONNECTIONS, 0, "",  "randomize-connections",  option::Arg::None, "  --randomize-connections   Randomize connections of generated topology"},
    {0, 0, 0, 0, 0, 0}
  };

  option::Stats stats(usage, argc-1, argv+1);
  std::vector<option::Option> options(stats.options_max);
  std::vector<option::Option> buffer(stats.options_max);
  option::Parser parse(usage, argc-1, argv+1, &options.at(0), &buffer.at(0));

  if (parse.error()) { return 1; }

  if (options[HELP])
  {
    option::printUsage(std::cout, usage);
    return 0;
  }

  if (options[UNKNOWN])
  {
    option::printUsage(std::cout, usage);
    std::cout << std::endl;
    std::cerr << "Unknown option: " << options[UNKNOWN].name << std::endl;
    return 1;
  }

  vtkm::IdComponent numTrials = 5;
  if (options[TRIALS])
  {
    numTrials = static_cast<vtkm::IdComponent>(std::stoi(options[TRIALS].arg));
  }

  vtkm::Id3 gridSize(100);

  if (options[SIZE])
  {
    gridSize = vtkm::Id3(std::stoi(options[SIZE].arg));
  }
  if (options[SIZEX])
  {
    gridSize[0] = std::stoi(options[SIZEX].arg);
  }
  if (options[SIZEY])
  {
    gridSize[1] = std::stoi(options[SIZEY].arg);
  }
  if (options[SIZEZ])
  {
    gridSize[2] = std::stoi(options[SIZEZ].arg);
  }

  YamlWriter log;
  log.StartListItem();

  log.AddDictionaryEntry("vtkm-version", VTKM_VERSION_FULL);

#if _WIN32
  TCHAR hostname[MAX_COMPUTERNAME_LENGTH+1];
  DWORD hostnameSize = sizeof(hostname) / sizeof(TCHAR);
  GetComputerName(hostname, &hostnameSize);
#else
  char hostname[256];
  gethostname(hostname, 256);
#endif
  log.AddDictionaryEntry("hostname", hostname);

  log.AddDictionaryEntry(
        "device",
        vtkm::cont::DeviceAdapterTraits<VTKM_DEFAULT_DEVICE_ADAPTER_TAG>::GetName());
  vtkm::cont::GetGlobalRuntimeDeviceTracker().ForceDevice(
        VTKM_DEFAULT_DEVICE_ADAPTER_TAG());

  std::time_t currentTime = std::time(nullptr);
  char timeString[256];
  std::strftime(timeString,
                256,
                "%Y-%m-%dT%H:%M:%S%z",
                std::localtime(&currentTime));
  log.AddDictionaryEntry("date", timeString);

  log.AddDictionaryEntry("size-x", gridSize[0]);
  log.AddDictionaryEntry("size-y", gridSize[1]);
  log.AddDictionaryEntry("size-z", gridSize[2]);

  vtkm::cont::DataSet inputData = CreateDataSet(gridSize);
  if (options[RANDOMIZE_CONNECTIONS])
  {
    log.AddDictionaryEntry("topology-connections", "randomized");
    inputData = RandomizeDataSet(inputData, log);
  }
  else
  {
    log.AddDictionaryEntry("topology-connections", "regular");
  }

  log.AddDictionaryEntry("num-input-cells",
                           inputData.GetCellSet().GetNumberOfCells());

  log.StartBlock("experiments");

  DoRun(vtkm::worklet::ExternalFacesSortId3(),
        "Sort",
        "Id3",
        numTrials,
        inputData,
        log);
  DoRun(vtkm::worklet::ExternalFacesSortIdXor(),
        "Sort",
        "XOR Indices",
        numTrials,
        inputData,
        log);
  DoRun(vtkm::worklet::ExternalFacesSortFnv1a(),
        "Sort",
        "FNV1A",
        numTrials,
        inputData,
        log);
  DoRun(vtkm::worklet::ExternalFacesSortMorton(),
        "Sort",
        "Morton",
        numTrials,
        inputData,
        log);
  DoRun(vtkm::worklet::ExternalFacesHashFightIdXor(),
        "Hash Fight",
        "XOR Indices",
        numTrials,
        inputData,
        log);
  DoRun(vtkm::worklet::ExternalFacesHashFightFnv1a(),
        "Hash Fight",
        "FNV1A",
        numTrials,
        inputData,
        log);
  DoRun(vtkm::worklet::ExternalFacesHashFightMorton(),
        "Hash Fight",
        "Morton",
        numTrials,
        inputData,
        log);

  log.EndBlock();

  return 0;
}
