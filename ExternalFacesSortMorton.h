//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2014 Sandia Corporation.
//  Copyright 2014 UT-Battelle, LLC.
//  Copyright 2014 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_worklet_ExternalFacesSortMorton_h
#define vtk_m_worklet_ExternalFacesSortMorton_h

#include <vtkm/CellShape.h>
#include <vtkm/Math.h>

#include <vtkm/exec/CellFace.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/ArrayHandleIndex.h>
#include <vtkm/cont/ArrayHandlePermutation.h>
#include <vtkm/cont/ArrayHandleGroupVecVariable.h>
#include <vtkm/cont/CellSetExplicit.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/Field.h>
#include <vtkm/cont/Timer.h>

#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/DispatcherReduceByKey.h>
#include <vtkm/worklet/Keys.h>
#include <vtkm/worklet/ScatterCounting.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/WorkletReduceByKey.h>

// If moved to VTK-m, this should become #include <vtkm/MortonCodes.h>
#include "MortonCodes.h"

#include "YamlWriter.h"

namespace vtkm
{
namespace worklet
{

struct ExternalFacesSortMorton
{
  //Worklet that returns the number of faces for each cell/shape
  class NumFacesPerCell : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    typedef void ControlSignature(CellSetIn inCellSet,
                                  FieldOut<> numFacesInCell);
    typedef _2 ExecutionSignature(CellShape);
    typedef _1 InputDomain;

    template<typename CellShapeTag>
    VTKM_EXEC
    vtkm::IdComponent operator()(CellShapeTag shape) const
    {
      return vtkm::exec::CellFaceNumberOfFaces(shape, *this);
    }
  };

  //Worklet that computes the Morton code for every point
  class PointMorton : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> pointCoordinates,
                                  FieldOut<> mortonCode);
    typedef _2 ExecutionSignature(_1);
    using InputDomain = _1;

    VTKM_CONT
    PointMorton(const vtkm::Bounds &bounds)
      : Bounds(bounds)
    {  }

    template<typename CoordinateValueType>
    VTKM_EXEC
    vtkm::UInt32 operator()(const vtkm::Vec<CoordinateValueType,3> &coords) const
    {
      return vtkm::Morton32(coords, this->Bounds);
    }

  private:
    vtkm::Bounds Bounds;
  };

  //Worklet that identifies a cell face by an XOR of all point ids. Not
  //necessarily completely unique.
  class FaceHash : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    typedef void ControlSignature(CellSetIn cellset,
                                  FieldInPoint<> pointMorton,
                                  FieldOut<> faceHashes,
                                  FieldOut<> originCells,
                                  FieldOut<> originFaces);
    typedef void ExecutionSignature(_2,
                                    _3,
                                    _4,
                                    _5,
                                    CellShape,
                                    InputIndex,
                                    VisitIndex);
    typedef _1 InputDomain;

    using ScatterType = vtkm::worklet::ScatterCounting;

    template<typename PointMortonVecType,
             typename CellShapeTag>
    VTKM_EXEC
    void operator()(const PointMortonVecType &pointMorton,
                    vtkm::UInt32 &faceHash,
                    vtkm::Id &cellIndex,
                    vtkm::IdComponent &faceIndex,
                    CellShapeTag shape,
                    vtkm::Id inputIndex,
                    vtkm::IdComponent visitIndex) const
    {
      // The basic idea of the hash is to have the morton code for the point
      // coordinates at the center of the face. What we compute is slightly
      // different though. We are computing the Morton code for each point and
      // then adding them together. This is sort of a sum rather than an
      // average of point coordinates (with some other minor differences), but
      // the real point is that the operation will be the same for all sets of
      // points.
      faceHash = 0;

      const vtkm::IdComponent numPointsInFace =
        vtkm::exec::CellFaceNumberOfPoints(visitIndex, shape, *this);

      for (vtkm::IdComponent pointIndex = 0;
           pointIndex < numPointsInFace;
           pointIndex++)
      {
        vtkm::IdComponent localPointId = vtkm::exec::CellFaceLocalIndex(pointIndex, visitIndex,
                                                                        shape, *this);

        vtkm::UInt32 pointHash = pointMorton[localPointId];
        faceHash += pointHash;
      }

      cellIndex = inputIndex;
      faceIndex = visitIndex;
    }
  };

  // Worklet that identifies the number of cells written out per face.
  // Because there can be collisions in the face ids, this instance might
  // represent multiple faces, which have to be checked. The resulting
  // number is the total number of external faces.
  class FaceCounts : public vtkm::worklet::WorkletReduceByKey
  {
  public:
    typedef void ControlSignature(KeysIn keys,
                                  WholeCellSetIn<> inputCells,
                                  ValuesIn<> originCells,
                                  ValuesIn<> originFaces,
                                  ReducedValuesOut<> numOutputCells);
    typedef _5 ExecutionSignature(_2, _3, _4);
    using InputDomain = _1;

    template<typename CellSetType,
             typename OriginCellsType,
             typename OriginFacesType>
    VTKM_EXEC
    vtkm::IdComponent operator()(const CellSetType &cellSet,
                                 const OriginCellsType &originCells,
                                 const OriginFacesType &originFaces) const
    {
      vtkm::IdComponent numCellsOnHash = originCells.GetNumberOfComponents();
      VTKM_ASSERT(originFaces.GetNumberOfComponents() == numCellsOnHash);

      // Start by assuming all faces are unique, then remove one for each
      // face we find a duplicate for.
      vtkm::IdComponent numExternalFaces = numCellsOnHash;

      for (vtkm::IdComponent myIndex = 0;
           myIndex < numCellsOnHash-1; // Don't need to check last face
           myIndex++)
      {
        vtkm::Id3 myFace = vtkm::exec::CellFaceCanonicalId(originFaces[myIndex],
                                                           cellSet.GetCellShape(originCells[myIndex]),
                                                           cellSet.GetIndices(originCells[myIndex]),
                                                           *this);

        for (vtkm::IdComponent otherIndex = myIndex+1;
             otherIndex < numCellsOnHash;
             otherIndex++)
        {
          vtkm::Id3 otherFace = vtkm::exec::CellFaceCanonicalId(originFaces[otherIndex],
                                                                cellSet.GetCellShape(originCells[otherIndex]),
                                                                cellSet.GetIndices(originCells[otherIndex]),
                                                                *this);
          if (myFace == otherFace)
          {
            // Faces are the same. Must be internal. Remove 2, one for each
            // face.
            numExternalFaces -= 2;
            break;
          }
        }
      }

      return numExternalFaces;
    }
  };

  // Worklet that returns the number of points for each outputted face
  class NumPointsPerFace : public vtkm::worklet::WorkletReduceByKey
  {
  public:
    typedef void ControlSignature(KeysIn keys,
                                  WholeCellSetIn<> inputCells,
                                  ValuesIn<> originCells,
                                  ValuesIn<> originFaces,
                                  ReducedValuesOut<> numPointsInFace);
    typedef _5 ExecutionSignature(_2, _3, _4, VisitIndex);
    using InputDomain = _1;

    using ScatterType = vtkm::worklet::ScatterCounting;

    template<typename CellSetType,
             typename OriginCellsType,
             typename OriginFacesType>
    VTKM_EXEC
    vtkm::IdComponent operator()(const CellSetType &cellSet,
                                 const OriginCellsType &originCells,
                                 const OriginFacesType &originFaces,
                                 vtkm::IdComponent visitIndex) const
    {
      vtkm::IdComponent numCellsOnHash = originCells.GetNumberOfComponents();
      VTKM_ASSERT(originFaces.GetNumberOfComponents() == numCellsOnHash);

      // Find the visitIndex-th unique face.
      vtkm::IdComponent numFound = 0;
      vtkm::IdComponent myIndex = 0;
      while (true)
      {
        vtkm::Id3 myFace = vtkm::exec::CellFaceCanonicalId(originFaces[myIndex],
                                                           cellSet.GetCellShape(originCells[myIndex]),
                                                           cellSet.GetIndices(originCells[myIndex]),
                                                           *this);
        bool foundPair = false;
        for (vtkm::IdComponent otherIndex = myIndex+1;
             otherIndex < numCellsOnHash;
             otherIndex++)
        {
          vtkm::Id3 otherFace = vtkm::exec::CellFaceCanonicalId(originFaces[otherIndex],
                                                                cellSet.GetCellShape(originCells[otherIndex]),
                                                                cellSet.GetIndices(originCells[otherIndex]),
                                                                *this);

          if (myFace == otherFace)
          {
            // Faces are the same. Must be internal.
            foundPair = true;
            break;
          }
        }

        if (!foundPair)
        {
          if (numFound == visitIndex)
          {
            break;
          }
          else
          {
            numFound++;
          }
        }

        myIndex++;
      }

      return vtkm::exec::CellFaceNumberOfPoints(
            originFaces[myIndex],
            cellSet.GetCellShape(originCells[myIndex]),
            *this);
    }
  };

  // Worklet that returns the shape and connectivity for each external face
  class BuildConnectivity : public vtkm::worklet::WorkletReduceByKey
  {
  public:
    typedef void ControlSignature(KeysIn keys,
                                  WholeCellSetIn<> inputCells,
                                  ValuesIn<> originCells,
                                  ValuesIn<> originFaces,
                                  ReducedValuesOut<> shapesOut,
                                  ReducedValuesOut<> connectivityOut);
    typedef void ExecutionSignature(_2, _3, _4, VisitIndex, _5, _6);
    using InputDomain = _1;

    using ScatterType = vtkm::worklet::ScatterCounting;

    template<typename CellSetType,
             typename OriginCellsType,
             typename OriginFacesType,
             typename ConnectivityType>
    VTKM_EXEC
    void operator()(const CellSetType &cellSet,
                    const OriginCellsType &originCells,
                    const OriginFacesType &originFaces,
                    vtkm::IdComponent visitIndex,
                    vtkm::UInt8 &shapeOut,
                    ConnectivityType &connectivityOut) const
    {
      vtkm::IdComponent numCellsOnHash = originCells.GetNumberOfComponents();
      VTKM_ASSERT(originFaces.GetNumberOfComponents() == numCellsOnHash);

      // Find the visitIndex-th unique face.
      vtkm::IdComponent numFound = 0;
      vtkm::IdComponent myIndex = 0;
      while (true)
      {
        vtkm::Id3 myFace = vtkm::exec::CellFaceCanonicalId(originFaces[myIndex],
                                                           cellSet.GetCellShape(originCells[myIndex]),
                                                           cellSet.GetIndices(originCells[myIndex]),
                                                           *this);
        bool foundPair = false;
        for (vtkm::IdComponent otherIndex = myIndex+1;
             otherIndex < numCellsOnHash;
             otherIndex++)
        {
          vtkm::Id3 otherFace = vtkm::exec::CellFaceCanonicalId(originFaces[otherIndex],
                                                                cellSet.GetCellShape(originCells[otherIndex]),
                                                                cellSet.GetIndices(originCells[otherIndex]),
                                                                *this);
          if (myFace == otherFace)
          {
            // Faces are the same. Must be internal.
            foundPair = true;
            break;
          }
        }

        if (!foundPair)
        {
          if (numFound == visitIndex)
          {
            break;
          }
          else
          {
            numFound++;
          }
        }

        myIndex++;
      }

      typename CellSetType::CellShapeTag shapeIn =
          cellSet.GetCellShape(originCells[myIndex]);
      shapeOut = vtkm::exec::CellFaceShape(originFaces[myIndex], shapeIn, *this);

      const vtkm::IdComponent numFacePoints =
        vtkm::exec::CellFaceNumberOfPoints(originFaces[myIndex], shapeIn, *this);

      typename CellSetType::IndicesType inCellIndices =
          cellSet.GetIndices(originCells[myIndex]);

      for (vtkm::IdComponent facePointIndex = 0;
           facePointIndex < numFacePoints;
           facePointIndex++)
      {
        vtkm::IdComponent localPointId = vtkm::exec::CellFaceLocalIndex(facePointIndex, originFaces[myIndex],
                                                                        shapeIn, *this);

        connectivityOut[facePointIndex] =
            inCellIndices[localPointId];
      }
    }
  };

public:

  ///////////////////////////////////////////////////
  /// \brief ExternalFacesSortMorton: Extract Faces on outside of geometry
  template <typename InCellSetType,
            typename PointCoordsType,
            typename ShapeStorage,
            typename NumIndicesStorage,
            typename ConnectivityStorage,
            typename OffsetsStorage,
            typename DeviceAdapter>
  VTKM_CONT
  void Run(const InCellSetType &inCellSet,
           const PointCoordsType &pointCoords,
           vtkm::cont::CellSetExplicit<
             ShapeStorage,
             NumIndicesStorage,
             ConnectivityStorage,
             OffsetsStorage> &outCellSet,
           YamlWriter &log,
           DeviceAdapter)
  {
    // Not the best place to call GetBounds. Probably should eventually get
    // it passed in.
    vtkm::Bounds bounds = pointCoords.GetBounds();

    //Create a worklet to map the number of faces to each cell
    vtkm::cont::ArrayHandle<vtkm::IdComponent> facesPerCell;
    vtkm::worklet::DispatcherMapTopology<NumFacesPerCell,DeviceAdapter>
        numFacesDispatcher;

    vtkm::cont::Timer<DeviceAdapter> timer;
    numFacesDispatcher.Invoke(inCellSet, facesPerCell);
    log.AddDictionaryEntry("seconds-num-faces-per-cell",
                           timer.GetElapsedTime());

    timer.Reset();
    vtkm::worklet::ScatterCounting
        scatterCellToFace(facesPerCell, DeviceAdapter());
    log.AddDictionaryEntry("seconds-face-input-count", timer.GetElapsedTime());
    facesPerCell.ReleaseResources();

    if (scatterCellToFace.GetOutputRange(inCellSet.GetNumberOfCells()) == 0)
    {
      // Data has no faces. Output is empty.
      outCellSet.PrepareToAddCells(0, 0);
      outCellSet.CompleteAddingCells(inCellSet.GetNumberOfPoints());
      return;
    }

    vtkm::cont::ArrayHandle<vtkm::UInt32> pointMorton;
    timer.Reset();
    vtkm::worklet::DispatcherMapField<PointMorton,DeviceAdapter>
        pointMortonDispatcher((PointMorton(bounds)));
    pointMortonDispatcher.Invoke(pointCoords, pointMorton);
    log.AddDictionaryEntry("seconds-morton-codes", timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::UInt32> faceHashes;
    vtkm::cont::ArrayHandle<vtkm::Id> originCells;
    vtkm::cont::ArrayHandle<vtkm::IdComponent> originFaces;

    vtkm::worklet::DispatcherMapTopology<FaceHash,DeviceAdapter>
        faceHashDispatcher(scatterCellToFace);

    timer.Reset();
    faceHashDispatcher.Invoke(
          inCellSet, pointMorton, faceHashes, originCells, originFaces);
    log.AddDictionaryEntry("seconds-face-hash", timer.GetElapsedTime());
    pointMorton.ReleaseResources();

    timer.Reset();
    vtkm::worklet::Keys<vtkm::UInt32> faceKeys(faceHashes,DeviceAdapter());
    log.AddDictionaryEntry("seconds-keys-build-arrays",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::IdComponent> faceOutputCount;
    vtkm::worklet::DispatcherReduceByKey<FaceCounts,DeviceAdapter>
        faceCountDispatcher;

    timer.Reset();
    faceCountDispatcher.Invoke(faceKeys,
                               inCellSet,
                               originCells,
                               originFaces,
                               faceOutputCount);
    log.AddDictionaryEntry("seconds-face-count", timer.GetElapsedTime());

    timer.Reset();
    vtkm::worklet::ScatterCounting
        scatterCullInternalFaces(faceOutputCount, DeviceAdapter());
    log.AddDictionaryEntry("seconds-face-output-count",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::IdComponent,NumIndicesStorage> facePointCount;
    vtkm::worklet::DispatcherReduceByKey<NumPointsPerFace,DeviceAdapter>
        pointsPerFaceDispatcher(scatterCullInternalFaces);

    timer.Reset();
    pointsPerFaceDispatcher.Invoke(faceKeys,
                                   inCellSet,
                                   originCells,
                                   originFaces,
                                   facePointCount);
    log.AddDictionaryEntry("seconds-points-per-face",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::UInt8,ShapeStorage> faceShapes;

    vtkm::cont::ArrayHandle<vtkm::Id,OffsetsStorage> faceOffsets;
    vtkm::Id connectivitySize;
    timer.Reset();
    vtkm::cont::ConvertNumComponentsToOffsets(
          facePointCount, faceOffsets, connectivitySize);
    log.AddDictionaryEntry("seconds-face-point-count",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::Id,ConnectivityStorage> faceConnectivity;
    // Must pre allocate because worklet invocation will not have enough
    // information to.
    faceConnectivity.Allocate(connectivitySize);

    vtkm::worklet::DispatcherReduceByKey<BuildConnectivity,DeviceAdapter>
        buildConnectivityDispatcher(scatterCullInternalFaces);

    timer.Reset();
    buildConnectivityDispatcher.Invoke(
          faceKeys,
          inCellSet,
          originCells,
          originFaces,
          faceShapes,
          vtkm::cont::make_ArrayHandleGroupVecVariable(faceConnectivity,
                                                       faceOffsets));
    log.AddDictionaryEntry("seconds-build-connectivity",
                           timer.GetElapsedTime());

    outCellSet.Fill(inCellSet.GetNumberOfPoints(),
                    faceShapes,
                    facePointCount,
                    faceConnectivity,
                    faceOffsets);
  }

}; //struct ExternalFacesSortMorton


}} //namespace vtkm::worklet

#endif //vtk_m_worklet_ExternalFacesSortMorton_h
