//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2014 Sandia Corporation.
//  Copyright 2014 UT-Battelle, LLC.
//  Copyright 2014 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_worklet_ExternalFacesHashFightIdXor_h
#define vtk_m_worklet_ExternalFacesHashFightIdXor_h

#include <vtkm/CellShape.h>
#include <vtkm/Math.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/ArrayHandleCounting.h>
#include <vtkm/cont/ArrayHandleImplicit.h>
#include <vtkm/cont/ArrayHandleIndex.h>
#include <vtkm/cont/ArrayHandlePermutation.h>
#include <vtkm/cont/CellSetExplicit.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/Field.h>
#include <vtkm/cont/Timer.h>

#include <vtkm/exec/CellFace.h>

#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/ScatterCounting.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/WorkletMapTopology.h>

#include "YamlWriter.h"

namespace vtkm
{
namespace worklet
{

struct ExternalFacesHashFightIdXor
{
  //Unary predicate operator
  //Returns True if the argument is equal to the constructor
  //integer argument; False otherwise.
  struct IsIntValue
  {
  private:
    int Value;

  public:
    VTKM_EXEC_CONT
    IsIntValue(const int &v) : Value(v) { }

    template<typename T>
    VTKM_EXEC_CONT
    bool operator()(const T &x) const
    {
      return x == T(Value);
    }
  };

  //Worklet that returns the number of faces for each cell/shape
  class NumFacesPerCell : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    typedef void ControlSignature(CellSetIn inCellSet,
                                  FieldOut<> numFacesInCell);
    typedef _2 ExecutionSignature(CellShape);
    typedef _1 InputDomain;

    template<typename CellShapeTag>
    VTKM_EXEC
    vtkm::IdComponent operator()(CellShapeTag shape) const
    {
      return vtkm::exec::CellFaceNumberOfFaces(shape, *this);
    }
  };

  //Worklet that generates a hash value for each face. It also has the lookup
  //to the origin cell/face
  class FaceHash : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    typedef void ControlSignature(CellSetIn cellset,
                                  FieldOut<> faceHashes,
                                  FieldOut<> originCells,
                                  FieldOut<> originFaces);
    typedef void ExecutionSignature(_2,
                                    _3,
                                    _4,
                                    CellShape,
                                    FromIndices,
                                    InputIndex,
                                    VisitIndex);
    typedef _1 InputDomain;

    using ScatterType = vtkm::worklet::ScatterCounting;

    template<typename CellShapeTag,
             typename CellNodeVecType>
    VTKM_EXEC
    void operator()(vtkm::UInt32 &faceHash,
                    vtkm::Id &cellIndex,
                    vtkm::IdComponent &faceIndex,
                    CellShapeTag shape,
                    const CellNodeVecType &cellNodeIds,
                    vtkm::Id inputIndex,
                    vtkm::IdComponent visitIndex) const
    {
      cellIndex = inputIndex;
      faceIndex = visitIndex;

      faceHash = 0;

      const vtkm::IdComponent numPointsInFace =
         vtkm::exec::CellFaceNumberOfPoints(visitIndex, shape, *this);

      for (vtkm::IdComponent pointIndex = 0;
           pointIndex < numPointsInFace;
           pointIndex++)
      {
        vtkm::IdComponent localId = 
                          vtkm::exec::CellFaceLocalIndex(pointIndex, visitIndex, shape, *this);

        // It is possible we are truncating the high order bits of the id.
        // Since the hash does not need to be perfect, we are going to live
        // with that.
        vtkm::UInt32 pointId = static_cast<vtkm::UInt32>(cellNodeIds[localId]);
        faceHash ^= pointId;
      }
    }
  };

  //Worklet that writes the face index at the location of the hash table.
  //Multiple entries are likely to write to the hash table, so they fight
  //and (hopefully) one wins.
  class HashFight : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> Hashes,
                                  FieldIn<> FaceIds,
                                  WholeArrayOut<> HashTable);
    typedef void ExecutionSignature(_1, _2, _3);

    VTKM_CONT
    HashFight(vtkm::Id hashTableSize)
      : HashTableSize(hashTableSize)
    {  }

    template<typename HashTablePortalType>
    VTKM_EXEC
    void operator()(vtkm::Id hash,
                    vtkm::Id faceId,
                    HashTablePortalType &hashTablePortal) const
    {
      hashTablePortal.Set(hash%this->HashTableSize, faceId);
    }

  private:
    vtkm::Id HashTableSize;
  };

  //Worklet that detects whether a face is internal.  If the
  //face is internal, then a value should not be assigned to the
  //face in the output array handle of face vertices; only external
  //faces should have a vector not equal to <-1,-1,-1>
  class CheckForMatches : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> activeHashes,
                                  FieldIn<> activeFaceIndices,
                                  WholeCellSetIn<> cellSet,
                                  WholeArrayIn<> originCells,
                                  WholeArrayIn<> originFaces,
                                  WholeArrayIn<> hashTable,
                                  FieldInOut<> isInactive,
                                  WholeArrayInOut<> isExternalFace);
    typedef void ExecutionSignature(_1, _2, _3, _4, _5, _6, _7, _8);

    VTKM_CONT
    CheckForMatches(vtkm::Id hashTableSize)
      : HashTableSize(hashTableSize)
    {  }

    template<typename CellSetType,
             typename OriginCellsPortal,
             typename OriginFacesPortal,
             typename HashTablePortal,
             typename IsExternalFacePortal>
    VTKM_EXEC
    void operator()(vtkm::UInt32 hash,
                    vtkm::Id faceIndex,
                    const CellSetType &cellSet,
                    const OriginCellsPortal &originCellsPortal,
                    const OriginFacesPortal &originFacesPortal,
                    const HashTablePortal &hashTablePortal,
                    vtkm::UInt8 &isInactive,
                    IsExternalFacePortal &isExternalFacePortal) const
    {
      vtkm::Id hashWinnerFace = hashTablePortal.Get(hash%this->HashTableSize);

      if (hashWinnerFace == faceIndex)
      {
        // Case 1: I won the hash fight by writing my index. I'm done so mark
        // myself as inactive.
        isInactive = vtkm::UInt8(1);
      }
      else
      {
        // Get a cononical representation of my face.
        vtkm::Id myOriginCell = originCellsPortal.Get(faceIndex);
	vtkm::IdComponent myOriginFace = originFacesPortal.Get(faceIndex);

        vtkm::Id3 myFace = vtkm::exec::CellFaceCanonicalId(myOriginFace,
                                                           cellSet.GetCellShape(myOriginCell),
                                                           cellSet.GetIndices(myOriginCell),
                                                           *this);

        // Get a cononical representation of the face in the hash table.
        vtkm::Id otherOriginCell = originCellsPortal.Get(hashWinnerFace);
        vtkm::IdComponent otherOriginFace = originFacesPortal.Get(hashWinnerFace);

        vtkm::Id3 otherFace = vtkm::exec::CellFaceCanonicalId(otherOriginFace,
                                                           cellSet.GetCellShape(otherOriginCell),
                                                           cellSet.GetIndices(otherOriginCell),
                                                           *this);

        // See if these are the same face
        if (myFace == otherFace)
        {
          // Case 2: The faces are the same. This must be an internal face.
          // Mark both myself and the other face as internal.
          isInactive = vtkm::UInt8(1);
          isExternalFacePortal.Set(faceIndex, vtkm::UInt8(0));
          isExternalFacePortal.Set(hashWinnerFace, vtkm::UInt8(0));
        }
        else
        {
          // Case 3: I didn't win and my face didn't match. I didn't learn
          // anything so do nothing.
        }
      }
    }

  private:
    vtkm::Id HashTableSize;
  };

  // Worklet that counts the number of points that are in each (active) face.
  class NumPointsPerFace : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> faceIndices,
                                  WholeCellSetIn<> cellSet,
                                  WholeArrayIn<> originCells,
                                  WholeArrayIn<> originFaces,
                                  FieldOut<> numPointsInFace);
    typedef _5 ExecutionSignature(_1, _2, _3, _4);

    using ScatterType = vtkm::worklet::ScatterCounting;

    template<typename CellSetType,
             typename OriginCellsPortalType,
             typename OriginFacesPortalType>
    VTKM_EXEC
    vtkm::IdComponent operator()(vtkm::Id faceIndex,
                                 const CellSetType &cellSet,
                                 const OriginCellsPortalType &originCellsPortal,
                                 const OriginFacesPortalType &originFacesPortal
                                 ) const
    {
      vtkm::Id originCell = originCellsPortal.Get(faceIndex);
      vtkm::IdComponent originFace = originFacesPortal.Get(faceIndex);
      return vtkm::exec::CellFaceNumberOfPoints(
            originFace, cellSet.GetCellShape(originCell), *this);
    }
  };

  // Worklet that writes out the shape and indices for each (active) face.
  class BuildConnectivity : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> faceIndices,
                                  WholeCellSetIn<> cellSet,
                                  WholeArrayIn<> originCells,
                                  WholeArrayIn<> originFaces,
                                  FieldOut<> shapesOut,
                                  FieldOut<> connectivityOut);
    typedef void ExecutionSignature(_1, _2, _3, _4, _5, _6);

    using ScatterType = vtkm::worklet::ScatterCounting;

    template<typename CellSetType,
             typename OriginCellsPortalType,
             typename OriginFacesPortalType,
             typename ConnectivityType>
    VTKM_EXEC
    void operator()(vtkm::Id faceIndex,
                    const CellSetType &cellSet,
                    const OriginCellsPortalType &originCellsPortal,
                    const OriginFacesPortalType &originFacesPortal,
                    vtkm::UInt8 &shapeOut,
                    ConnectivityType &connectivityOut) const
    {
      vtkm::Id originCell = originCellsPortal.Get(faceIndex);
      vtkm::IdComponent originFace = originFacesPortal.Get(faceIndex);

      shapeOut = vtkm::exec::CellFaceShape(originFace,
                                           cellSet.GetCellShape(originCell),
                                           *this);

     const vtkm::IdComponent numFacePoints =
        vtkm::exec::CellFaceNumberOfPoints(originFace, cellSet.GetCellShape(originCell), *this);

      VTKM_ASSERT(numFacePoints == connectivityOut.GetNumberOfComponents());

      typename CellSetType::IndicesType inCellIndices =
          cellSet.GetIndices(originCell);

      for (vtkm::IdComponent facePointIndex = 0;
           facePointIndex < numFacePoints;
           facePointIndex++)
      {
        vtkm::IdComponent localId =
                          vtkm::exec::CellFaceLocalIndex(facePointIndex, originFace,
                                                         cellSet.GetCellShape(originCell), *this);

        connectivityOut[facePointIndex] =
            inCellIndices[localId];
      }
    }
  };

public:

  ///////////////////////////////////////////////////
  /// \brief ExternalFacesHashFightIdXor: Extract Faces on outside of geometry
  template <typename InCellSetType,
            typename PointCoordsType,
            typename ShapeStorage,
            typename NumIndicesStorage,
            typename ConnectivityStorage,
            typename OffsetsStorage,
            typename DeviceAdapter>
  VTKM_CONT
  void Run(const InCellSetType &inCellSet,
           const PointCoordsType &vtkmNotUsed(pointCoords), // Remove if moved to VTK-m
           vtkm::cont::CellSetExplicit<
             ShapeStorage,
             NumIndicesStorage,
             ConnectivityStorage,
             OffsetsStorage> &outCellSet,
           YamlWriter &log,
           DeviceAdapter)
  {
    using Algorithm = vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>;

    //Create a worklet to map the number of faces to each cell
    vtkm::cont::ArrayHandle<vtkm::IdComponent> facesPerCell;
    vtkm::worklet::DispatcherMapTopology<NumFacesPerCell,DeviceAdapter>
        numFacesDispatcher;

    vtkm::cont::Timer<DeviceAdapter> timer;
    numFacesDispatcher.Invoke(inCellSet, facesPerCell);
    log.AddDictionaryEntry("seconds-num-faces-per-cell",
                           timer.GetElapsedTime());

    timer.Reset();
    vtkm::worklet::ScatterCounting
        scatterCellToFace(facesPerCell, DeviceAdapter());
    log.AddDictionaryEntry("seconds-face-input-count", timer.GetElapsedTime());
    facesPerCell.ReleaseResources();

    if (scatterCellToFace.GetOutputRange(inCellSet.GetNumberOfCells()) == 0)
    {
      // Data has no faces. Output is empty.
      outCellSet.PrepareToAddCells(0, 0);
      outCellSet.CompleteAddingCells(inCellSet.GetNumberOfPoints());
      return;
    }

    vtkm::cont::ArrayHandle<vtkm::UInt32> faceHashes;
    vtkm::cont::ArrayHandle<vtkm::Id> originCells;
    vtkm::cont::ArrayHandle<vtkm::IdComponent> originFaces;

    vtkm::worklet::DispatcherMapTopology<FaceHash,DeviceAdapter>
        faceHashDispatcher(scatterCellToFace);

    timer.Reset();
    faceHashDispatcher.Invoke(inCellSet, faceHashes, originCells, originFaces);
    log.AddDictionaryEntry("seconds-face-hash", timer.GetElapsedTime());

    vtkm::Id totalNumFaces = faceHashes.GetNumberOfValues();

    //----Begin Hashing Phase To Detect External Faces------------------//

    timer.Reset();

    //Set constant factor for hash table size: factor*totalFaces
    const vtkm::Id hashTableFactor = 2;

    vtkm::cont::ArrayHandle<vtkm::UInt8> isExternalFace;
    Algorithm::Copy(
          vtkm::cont::ArrayHandleConstant<vtkm::UInt8>(1, totalNumFaces),
          isExternalFace);

    vtkm::cont::ArrayHandle<vtkm::Id> activeFaceIndices;
    Algorithm::Copy(vtkm::cont::ArrayHandleIndex(totalNumFaces),
                    activeFaceIndices);

    vtkm::Id numActiveFaces = totalNumFaces;

    while (numActiveFaces > 0)
    {
      // Create a packe arrays of active face hashes
      auto activeHashes = vtkm::cont::make_ArrayHandlePermutation(activeFaceIndices, faceHashes);

      // Get ready the isInactive array.
      vtkm::cont::ArrayHandle<vtkm::UInt8> isInactive;
      Algorithm::Copy(
            vtkm::cont::ArrayHandleConstant<vtkm::UInt8>(0, numActiveFaces),
            isInactive);

      vtkm::Id hashTableSize = numActiveFaces * hashTableFactor;

      vtkm::cont::ArrayHandle<vtkm::Id> hashTable;
      hashTable.PrepareForOutput(hashTableSize, DeviceAdapter());

      // Have all active hashes try to write their index to the hash table
      vtkm::worklet::DispatcherMapField<HashFight,DeviceAdapter>
          fightDispatcher((HashFight(hashTableSize)));
      fightDispatcher.Invoke(activeHashes, activeFaceIndices, hashTable);


      // Have all active faces check to see if they matched and update
      // isInactive/isExternalFace.
      vtkm::worklet::DispatcherMapField<CheckForMatches,DeviceAdapter>
          matchDispatcher((CheckForMatches(hashTableSize)));
      matchDispatcher.Invoke(activeHashes,
                             activeFaceIndices,
                             inCellSet,
                             originCells,
                             originFaces,
                             hashTable,
                             isInactive,
                             isExternalFace);

      // Compact the activeFaceIndices by the isInactive flag.
      vtkm::cont::ArrayHandle<vtkm::Id> compactedActiveFaceIndices;
      Algorithm::CopyIf(activeFaceIndices,
                        isInactive,
                        compactedActiveFaceIndices,
                        IsIntValue(0));
      activeFaceIndices = compactedActiveFaceIndices;

      // Update the number of active faces
      numActiveFaces = activeFaceIndices.GetNumberOfValues();
    }

    log.AddDictionaryEntry("seconds-hash-fight-iterations",
                           timer.GetElapsedTime());

    //--------------End Hashing Phase-------------------------//

    timer.Reset();
    vtkm::worklet::ScatterCounting
        scatterCullInternalFaces(isExternalFace, DeviceAdapter());
    log.AddDictionaryEntry("seconds-face-output-count",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::IdComponent,NumIndicesStorage> facePointCount;

    vtkm::worklet::DispatcherMapField<NumPointsPerFace,DeviceAdapter>
        pointsPerFaceDispatcher(scatterCullInternalFaces);

    timer.Reset();
    pointsPerFaceDispatcher.Invoke(vtkm::cont::ArrayHandleIndex(totalNumFaces),
                                   inCellSet,
                                   originCells,
                                   originFaces,
                                   facePointCount);
    log.AddDictionaryEntry("seconds-points-per-face",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::UInt8,ShapeStorage> faceShapes;

    vtkm::cont::ArrayHandle<vtkm::Id,OffsetsStorage> faceOffsets;
    vtkm::Id connectivitySize;
    timer.Reset();
    vtkm::cont::ConvertNumComponentsToOffsets(
          facePointCount, faceOffsets, connectivitySize);
    log.AddDictionaryEntry("seconds-face-point-count",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::Id,ConnectivityStorage> faceConnectivity;
    // Must pre allocate because worklet invocation will not have enough
    // information to.
    faceConnectivity.PrepareForOutput(connectivitySize, DeviceAdapter());

    vtkm::worklet::DispatcherMapField<BuildConnectivity, DeviceAdapter>
        buildConnectivityDispatcher(scatterCullInternalFaces);

    timer.Reset();
    buildConnectivityDispatcher.Invoke(
          vtkm::cont::ArrayHandleIndex(totalNumFaces),
          inCellSet,
          originCells,
          originFaces,
          faceShapes,
          vtkm::cont::make_ArrayHandleGroupVecVariable(faceConnectivity,
                                                       faceOffsets));
    log.AddDictionaryEntry("seconds-build-connectivity",
                           timer.GetElapsedTime());

    outCellSet.Fill(inCellSet.GetNumberOfPoints(),
                    faceShapes,
                    facePointCount,
                    faceConnectivity,
                    faceOffsets);
  }

}; //struct ExternalFacesHashFightIdXor


}} //namespace vtkm::worklet

#endif //vtk_m_worklet_ExternalFacesHashFightIdXor_h
