//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2017 Sandia Corporation.
//  Copyright 2017 UT-Battelle, LLC.
//  Copyright 2017 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_MortonCodes_h
#define vtk_m_MortonCodes_h

#include <vtkm/Bounds.h>
#include <vtkm/Math.h>

namespace vtkm {

namespace detail {

//Note: if this takes a long time. we could use a lookup table
//expands 10-bit unsigned int into 30 bits
VTKM_EXEC
inline
vtkm::UInt32 ExpandBits32(vtkm::UInt32 x32)
{
  x32 = (x32 | (x32 << 16)) & 0x030000FF;
  x32 = (x32 | (x32 <<  8)) & 0x0300F00F;
  x32 = (x32 | (x32 <<  4)) & 0x030C30C3;
  x32 = (x32 | (x32 <<  2)) & 0x09249249;
  return x32;
}

VTKM_EXEC
inline
vtkm::UInt64 ExpandBits64(vtkm::UInt32 x)
{
  vtkm::UInt64 x64 = x & 0x1FFFFF;
  x64 = (x64 | x64 << 32) & 0x1F00000000FFFF;
  x64 = (x64 | x64 << 16) & 0x1F0000FF0000FF;
  x64 = (x64 | x64 << 8)  & 0x100F00F00F00F00F;
  x64 = (x64 | x64 << 4)  & 0x10c30c30c30c30c3;
  x64 = (x64 | x64 << 2)  & 0x1249249249249249;
  return x64;
}

} // namespace detail

// Returns a 30 bit morton code (in a 32-bit integer) for coordinates in the
// given spatial bounds
VTKM_EXEC
inline
vtkm::UInt32 Morton32(const vtkm::Vec<vtkm::Float64,3> &coord,
                      const vtkm::Bounds &bounds)
{
  // Rescale coordinates to unit cube
  vtkm::Vec<vtkm::Float64,3> unitCoords(
        (coord[0]-bounds.X.Min)/bounds.X.Length(),
        (coord[1]-bounds.Y.Min)/bounds.Y.Length(),
        (coord[2]-bounds.Z.Min)/bounds.Z.Length());

  // Scale again so that the truncated integer fits in 10 bits
  const vtkm::Float64 fixedPointScale = static_cast<vtkm::Float64>(1<<10);
  vtkm::Vec<vtkm::Float64,3> fixedPointCoords(
        vtkm::Min(vtkm::Max(unitCoords[0]*fixedPointScale, 0.0), fixedPointScale-1),
        vtkm::Min(vtkm::Max(unitCoords[1]*fixedPointScale, 0.0), fixedPointScale-1),
        vtkm::Min(vtkm::Max(unitCoords[2]*fixedPointScale, 0.0), fixedPointScale-1));

  // Expand the 10 bits to 30
  vtkm::UInt32 x = detail::ExpandBits32((vtkm::UInt32)fixedPointCoords[0]);
  vtkm::UInt32 y = detail::ExpandBits32((vtkm::UInt32)fixedPointCoords[1]);
  vtkm::UInt32 z = detail::ExpandBits32((vtkm::UInt32)fixedPointCoords[2]);

  // Interleave coordinates
  return (z << 2 | y << 1 | x);
}

VTKM_EXEC
inline
vtkm::UInt32 Morton32(const vtkm::Vec<vtkm::Float32,3> &coord,
                      const vtkm::Bounds &bounds)
{
  return Morton32(vtkm::Vec<vtkm::Float64,3>(coord), bounds);
}

VTKM_EXEC
inline
vtkm::UInt32 Morton32(vtkm::Float64 x,
                      vtkm::Float64 y,
                      vtkm::Float64 z,
                      const vtkm::Bounds &bounds)
{
  return Morton32(vtkm::make_Vec(x,y,z), bounds);
}

VTKM_EXEC
inline
vtkm::UInt32 Morton32(vtkm::Float32 x,
                      vtkm::Float32 y,
                      vtkm::Float32 z,
                      const vtkm::Bounds &bounds)
{
  return Morton32(vtkm::make_Vec(x,y,z), bounds);
}

// Returns a 63 bit morton code (in a 64-bit integer) for coordinates in the
// given spatial bounds
VTKM_EXEC
inline
vtkm::UInt64 Morton64(const vtkm::Vec<vtkm::Float64,3> &coord,
                      const vtkm::Bounds &bounds)
{
  // Rescale coordinates to unit cube
  vtkm::Vec<vtkm::Float64,3> unitCoords(
        (coord[0]-bounds.X.Min)/bounds.X.Length(),
        (coord[1]-bounds.Y.Min)/bounds.Y.Length(),
        (coord[2]-bounds.Z.Min)/bounds.Z.Length());

  // Scale again so that the truncated integer fits in 21 bits
  const vtkm::Float64 fixedPointScale = static_cast<vtkm::Float64>(1<<21);
  vtkm::Vec<vtkm::Float64,3> fixedPointCoords(
        vtkm::Min(vtkm::Max(unitCoords[0]*fixedPointScale, 0.0), fixedPointScale-1),
        vtkm::Min(vtkm::Max(unitCoords[1]*fixedPointScale, 0.0), fixedPointScale-1),
        vtkm::Min(vtkm::Max(unitCoords[2]*fixedPointScale, 0.0), fixedPointScale-1));

  // Expand the 10 bits to 30
  vtkm::UInt64 x = detail::ExpandBits64((vtkm::UInt32)fixedPointCoords[0]);
  vtkm::UInt64 y = detail::ExpandBits64((vtkm::UInt32)fixedPointCoords[1]);
  vtkm::UInt64 z = detail::ExpandBits64((vtkm::UInt32)fixedPointCoords[2]);

  // Interleave coordinates
  return (z << 2 | y << 1 | x);
}

VTKM_EXEC
inline
vtkm::UInt64 Morton64(const vtkm::Vec<vtkm::Float32,3> &coord,
                      const vtkm::Bounds &bounds)
{
  return Morton64(vtkm::Vec<vtkm::Float64,3>(coord), bounds);
}

VTKM_EXEC
inline
vtkm::UInt64 Morton64(vtkm::Float64 x,
                      vtkm::Float64 y,
                      vtkm::Float64 z,
                      const vtkm::Bounds &bounds)
{
  return Morton64(vtkm::make_Vec(x,y,z), bounds);
}

VTKM_EXEC
inline
vtkm::UInt64 Morton64(vtkm::Float32 x,
                      vtkm::Float32 y,
                      vtkm::Float32 z,
                      const vtkm::Bounds &bounds)
{
  return Morton64(vtkm::make_Vec(x,y,z), bounds);
}

} // namespace vtkm

#endif //vtk_m_MortonCodes_h
